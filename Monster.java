package com.edso.Bai47;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dev on 1/10/15.
 */
public class Monster implements ISaveable {
    private String name;
    private int hitPoints;
    private int strength;

    public Monster(String name, int hitPoints, int strength) {
        this.name = name;
        this.hitPoints = hitPoints;
        this.strength = strength;
    }

    public String getName() {
        return name;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public int getStrength() {
        return strength;
    }

    @Override
    public List<String> write() {
        ArrayList<String> listValues = new ArrayList<String>();
        listValues.add(0, this.name);
        listValues.add(1, "" + this.hitPoints);
        listValues.add(2, "" + this.strength);
        return listValues;
    }

    @Override
    public void read(List<String> listValues) {
        if(listValues != null && listValues.size() > 0) {
            this.name = listValues.get(0);
            this.hitPoints = Integer.parseInt(listValues.get(1));
            this.strength = Integer.parseInt(listValues.get(2));
        }

    }

    @Override
    public String toString() {
        return "Monster{" +
                "name='" + name + '\'' +
                ", hitPoints=" + hitPoints +
                ", strength=" + strength +
                '}';
    }
}
