package com.edso.Bai1;

import java.util.*;

public class ArrayEx {
    public static ArrayList<Double> randomArray(int length){

        ArrayList<Double> list = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            list.add(random.nextDouble()*100);
        }
        System.out.println(list);
        return list;
    }

    public static void remove(ArrayList<Double> list){

        Iterator<Double> iterator = list.iterator();
        while(iterator.hasNext()) {
            if(iterator.next() < 50) {
                iterator.remove();
            }
        }
        System.out.println(list);
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap so phan tu cua mang :");
        int n = scanner.nextInt();
        remove(randomArray(n));

    }
}
