package com.edso.Bai47;

import java.util.ArrayList;
import java.util.List;


public class Player implements ISaveable {
    private String name;
    private int hitPoints;
    private int strength;
    private String weapon;

    public Player(String name, int hitPoints, int strength) {
        this.name = name;
        this.hitPoints = hitPoints;
        this.strength = strength;
        this.weapon = "Sword";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", hitPoints=" + hitPoints +
                ", strength=" + strength +
                ", weapon='" + weapon + '\'' +
                '}';
    }

    @Override
    public List<String> write() {
        List<String> listValues = new ArrayList<String>();
        listValues.add( this.name);
        listValues.add("" + this.hitPoints);
        listValues.add("" + this.strength);
        listValues.add(this.weapon);

        return listValues;
    }

    @Override
    public void read(List<String> listValues) {
        if(listValues != null && listValues.size() >0) {
            this.name = listValues.get(0);
            this.hitPoints = Integer.parseInt(listValues.get(1));
            this.strength = Integer.parseInt(listValues.get(2));
            this.weapon = listValues.get(3);
        }

    }














}
